﻿using onlinefoodstore.Model;
using onlinefoodstore.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace onlinefoodstore.ViewModel
{
    public class LandingViewModel : BaseViewModel
    {
        public LandingViewModel()
        {
            burgers = GetBurgers();
        }

        ObservableCollection<Food> burgers;
        public ObservableCollection<Food> Burgers
        {
            get { return burgers; }
            set
            {
                burgers = value;
                OnPropertyChanged();
            }
        }

        private Food selectedBurger;
        public Food SelectedBurger
        {
            get { return selectedBurger; }
            set
            {
                selectedBurger = value;
                OnPropertyChanged();
            }
        }

        public ICommand SelectionCommand => new Command(DisplayBurger);

        private void DisplayBurger()
        {
            if (selectedBurger != null)
            {
                var viewModel = new DetailsViewModel { SelectedBurger = selectedBurger, Burgers = burgers, Position = burgers.IndexOf(selectedBurger) };
                var detailsPage = new DetailsPage { BindingContext = viewModel };

                var navigation = Application.Current.MainPage as NavigationPage;
                navigation.PushAsync(detailsPage, true);
            }
        }

        private ObservableCollection<Food> GetBurgers()
        {
            return new ObservableCollection<Food>
            {
                new Food { Name = "CLASSIC", Price = 12.99f, Image = "image6.jpg"},
                new Food { Name = "DOUBLE", Price = 19.99f, Image = "image7.jpg"},
                new Food { Name = "SHARK", Price = 17.29f, Image = "image8.jpg"},
                new Food { Name = "CHICKEN", Price = 15.99f, Image = "image9.png"},
                new Food{ Name = "MEAT", Price = 11.99f, Image = "image10.jpg"},
                new Food { Name = "BBQ", Price = 13.99f, Image = "image11.jpg"}
            };
        }
    }
}